<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShoppingListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 0; $i < 30; $i++) {

            DB::table('shopping_lists')->insert([
                'name' => $faker->word,
                'user_id' => $faker->unique()->numberBetween($min = 1, $max = 30),
                'completed' => $faker->boolean($chanceOfGettingTrue = 50),
            ]);
        }
    }
}
