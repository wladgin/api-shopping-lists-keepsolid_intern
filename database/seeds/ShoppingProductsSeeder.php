<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShoppingProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 0; $i < 30; $i++){

            DB::table('shopping_products')->insert([
                'name' => $faker->word,
                'quantity' =>$faker->numberBetween($min = 1, $max = 10),
                'product_group_id' =>$faker->numberBetween($min = 1, $max = 5),
                'shopping_list_id' =>$faker->numberBetween($min = 1, $max = 30),
                'completed' =>$faker->boolean($chanceOfGettingTrue = 50),
            ]);

        }
    }
}
