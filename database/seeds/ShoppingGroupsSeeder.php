<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShoppingGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        for($i = 0; $i < 5; $i++){

            DB::table('product_groups')->insert([
                'name' => $faker->word,
                'color_code' =>$faker->colorName,
            ]);

        }
    }
}
