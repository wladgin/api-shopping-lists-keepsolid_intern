<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shopping_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('quantity')->default(1);
            $table->bigInteger('product_group_id')->unsigned();
            $table->bigInteger('shopping_list_id')->unsigned();
            $table->boolean('completed')->default(false);
            $table->timestamps();

        });

        Schema::table('shopping_products', function (Blueprint $table){
            $table->foreign('product_group_id')->references('id')->on('product_groups');
            $table->foreign('shopping_list_id')->references('id')->on('shopping_lists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shopping_products');
    }
}
