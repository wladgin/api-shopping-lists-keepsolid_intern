<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/shopping-list/list', 'ShoppingList@list');

Route::prefix('auth')->group(function (){
   Route::post('register', 'AuthController@register')->name('auth.register');
   Route::post('login', 'AuthController@login')->name('auth.login');
});

Route::middleware('auth:api')->group(function (){
    Route::prefix('shopping-lists')->group(function (){
        Route::get('/', 'ShoppingListController@list');
        Route::get('/{id}', 'ShoppingListController@show');
        Route::post('/add', 'ShoppingListController@add');
        Route::post('/{id}/edit', 'ShoppingListController@edit');
        Route::post('/{id}/delete', 'ShoppingListController@delete');
    });

Route::post('shopping-list-invite', 'ShoppingListInvitesController@process');
});
/*Route::get('shopping-list/', 'ShoppingListController@showAll');
Route::get('shopping-list/create', 'ShoppingListController@create');
Route::post('shopping-list/create', 'ShoppingListController@store');
Route::get('shopping-list/edit/{id}', 'ShoppingListController@edit');
Route::get('shopping-list/edit/{id}', 'ShoppingListController@update');
Route::get('shopping-list/delete/{id}', 'ShoppingListController@destroy');*/
