<p>Hi,</p>

<p>Someone has invited you to access their Shopping List.</p>

<a href="{{ route('auth.register', ['invite' => $invite->invite_key]) }}">Click here</a> to activate!
