<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShoppingList extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function shoppingProducts(): HasMany
    {
        return $this->hasMany(ShoppingProduct::class, 'shopping_list_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function shoppingListInvite(): HasMany
    {
        return $this->hasMany(ShoppingListInvite::class);
    }
}
