<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * Class News
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @mixin Builder
 * @package App
 */
class ShoppingListInvite extends Model
{
    public const STATUS_INVITED = 0;
    public const STATUS_ACTIVE = 1;

    protected $fillable = [
        'email', 'shopping_list_id', 'invite_key', 'status', 'user_id'
    ];


    /**
     * @return BelongsTo
     */
    public function shopList(): BelongsTo
    {
        return $this->belongsTo(ShoppingList::class);
    }


    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
