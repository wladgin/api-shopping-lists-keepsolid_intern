<?php

namespace App\Http\Controllers;

use App\Mail\InviteCreated;
use App\ShoppingListInvite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ShoppingListInvitesController extends Controller
{


    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function process(Request $request): \Illuminate\Http\Response
    {

        $user = User::where('email', $request->get('email'))->first();

        $count = ShoppingListInvite::join('shopping_lists', 'shopping_list_id', '=', 'shopping_lists.id')
            ->where(['shopping_lists.user_id' => auth()->user()->id])
            ->count();

        if ($count >= 5) {
            return $this->respondWithError('Your count invite > 5', Response::HTTP_BAD_REQUEST);
        }
        // exist email and user_id?
        if ($user){
            ShoppingListInvite::create([
                'status' => ShoppingListInvite::STATUS_ACTIVE,
                'shopping_list_id' => $request->get('shopping_list_id'),
                'user_id' => $user->id,
            ]);
            return $this->respondWithData([]);
        }


        $invite = ShoppingListInvite::create([
            'invite_key' => Str::random(32),
            'status' => ShoppingListInvite::STATUS_INVITED,
            'shopping_list_id' => $request->get('shopping_list_id'),
        ]);

        // send the email
        Mail::to($request->get('email'))->send(new InviteCreated($invite));

        return $this->respondWithData([]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|null
     */
    public function accept(Request $request): ?\Illuminate\Http\Response
    {
        if(!$invite = ShoppingListInvite::where('token')->first()){
            return $this->respondWithError("Invite does not exist", Response::HTTP_NOT_FOUND);
        }

        User::create([
            'name' => $request->get('name'),
            'email' => $invite->email,
            'password' =>$request->get('password')
        ]);

        //update status
        $invite->update();


    }
}
