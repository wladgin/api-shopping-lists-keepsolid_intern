<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private const SUCCESS = 'success';
    private const MESSAGE = 'message';
    private const DATA = 'data';

    /**
     * @param array $data
     * @param $statusCode
     * @return Response
     */

    protected function respondWithData(array $data, $statusCode = Response::HTTP_OK)
    {
        return new Response([
            self::SUCCESS => true,
            self::DATA => $data,
        ], $statusCode);
    }

    /**
     * @param string $message
     * @param int $responseCode
     * @return Response
     */
    protected function respondWithError(string $message, int $responseCode): Response
    {
        return new Response([
            self::SUCCESS => false,
            self::MESSAGE => $message,
        ], $responseCode);
    }


    /**
     * @param string $message
     * @return Response
     */
    protected function respondNotFound(string $message): Response
    {
        return $this->respondWithError($message, Response::HTTP_NOT_FOUND);
    }
}
