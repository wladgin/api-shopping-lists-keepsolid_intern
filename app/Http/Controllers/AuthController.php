<?php

namespace App\Http\Controllers;

use App\ShoppingListInvite;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

class AuthController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return Response
     */
    public function register(RegisterRequest $request): Response
    {
        $parameters = $request->all();

        $user = new User();
        $user->fill($parameters);
        $user->password = Hash::make($request->password);
//
        try {
            if ($user->saveOrFail()) {
                if ($request->get('invite_key') && ($invite = ShoppingListInvite::where(['invite_key' => $request->invite_key, 'status' => ShoppingListInvite::STATUS_INVITED])->first())) {
                    $invite->status = ShoppingListInvite::STATUS_ACTIVE;
                    $invite->user_id = $user->id;
                    $invite->saveOrFail();
                }
            }
        } catch (\Throwable $exception) {
            return $this->respondWithError('Error saving user' . $exception->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }



        return $this->respondWithData($user->toArray());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function login(Request $request)
    {

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $user->setNewApiToken();
                $response = ['token' => $user->api_token];
                return response($response, 200);
            } else {
                return $this->respondWithError('Password missmatch', Response::HTTP_UNPROCESSABLE_ENTITY);
            }

        } else {
            return $this->respondWithError('User does not exist', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }
}
