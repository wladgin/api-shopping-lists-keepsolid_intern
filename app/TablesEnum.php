<?php
/**
 * Created by PhpStorm.
 * User: WladGita
 * Date: 19.07.2019
 * Time: 11:36
 */

namespace App;


use MyCLabs\Enum\Enum;

class TablesEnum extends Enum
{
    public const SHOPPING_LISTS = 'shopping_lists';
}
