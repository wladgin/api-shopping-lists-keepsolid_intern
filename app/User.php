<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * @return HasMany
     */
    public function shoppingLists(): HasMany
    {
        return $this->hasMany(ShoppingList::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function shopListInvite(): HasMany
    {
        return $this->hasMany(ShoppingListInvite::class);
    }

    /**
     * Create api token and save
     */
    public function setNewApiToken(): void
    {
        $this->api_token = Str::random(32);
        $this->save();
    }

}
